import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductoRoutingModule } from './producto-routing.module';
import { ListarProductosComponent } from './listar-productos/listar-productos.component';
import { ProductoComponent } from './producto/producto.component';
import { LayoutComponent } from './layout/layout.component';
import { PrimengModule } from '../primeng/primeng.module';
import { StoreModule } from '@ngrx/store';
import { productoReducer } from '../store/reducers';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    ListarProductosComponent,
    ProductoComponent,
    LayoutComponent
  ],
  imports: [
    CommonModule,
    ProductoRoutingModule,
    PrimengModule,
    RouterModule,
    ReactiveFormsModule,
    SharedModule,
  ]
})
export class ProductoModule { }
