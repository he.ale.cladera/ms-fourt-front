import { AutoCompleteCompleteEvent, AutoCompleteSelectEvent } from 'primeng/autocomplete';
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, inject } from '@angular/core';
import { MenuItem } from 'primeng/api';

import { Store } from '@ngrx/store';
import { Observable, Subject, takeUntil } from 'rxjs';

import { AppState } from '../../store/app.reducer';
import { cargarFiltroProductos, cargarProductos, cargarProductosPorFiltroSeleccionado, disponibilidadProducto } from '../../store/actions';
import { Producto } from '../../interfaces/producto.interface';
import { seleccionarFiltrosProductos, seleccionarProductos } from '../../store/selectors/productos.selector';

@Component({
  selector: 'app-listar-productos',
  templateUrl: './listar-productos.component.html',
  styleUrl: './listar-productos.component.css',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListarProductosComponent implements OnInit, OnDestroy{
  private destroy$     : Subject<void>;
  private store        : Store<AppState>;
  public disponibilidad: boolean;;
  public items         : MenuItem[];
  public loading$!     : Observable<boolean>;
  public page          : number;
  public productos$!   : Observable<Producto[]>;
  public rows          : number;
  public selectedValue!: string;
  public suggestions$! : Observable<string[]>;
  public totalElements!: number;

  constructor(){
    this.disponibilidad= true;
    this.page=0;
    this.rows=10;
    this.store= inject(Store);
    this.destroy$ = new Subject<void>();
    this.items = [
      {
        label: 'Opciones',
        items: [
          { label: 'Disponible', command: () => this.handleItemClick(true)},
          { label: 'No Disponible', command: () => this.handleItemClick(false)},
        ]
      }
    ]
  }

  ngOnInit(): void {
      this.store.select('productos').pipe(takeUntil(this.destroy$)).subscribe(data=>{
        this.totalElements= data.totalElements;
        this.page= data.page;
        this.rows= data.size;
        this.disponibilidad=data.disponibilidad;
      });
      this.productos$= this.store.select(seleccionarProductos).pipe(takeUntil(this.destroy$));
      this.suggestions$= this.store.select(seleccionarFiltrosProductos).pipe(takeUntil(this.destroy$));
      this.store.dispatch(cargarProductos({page: this.page, size: this.rows, disponibilidad: true}));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onPageChange(event: any) {
    this.store.dispatch(cargarProductos({page: event.page, size: event.rows , disponibilidad: this.disponibilidad}));
  }

  handleItemClick(event: boolean) {
    if (event){
      this.store.dispatch(cargarProductos({page: 0, size: this.rows , disponibilidad: event}));
    }else{
      this.store.dispatch(cargarProductos({page: 0, size: this.rows , disponibilidad: event}));
    }
  }

  setDisponibilidad(id:number){
    this.store.dispatch(disponibilidadProducto({idProducto:id,  page: this.page, size: this.rows, estado: this.disponibilidad}));
  }

  search(event: AutoCompleteCompleteEvent) {
    const query = event.query;
    if(query.length>=2 && query){
      this.store.dispatch(cargarFiltroProductos({terminoBuscar:query}));
    }
  }

  onSelect(event:AutoCompleteSelectEvent){
    const terminoBuscar= event.value;
    this.store.dispatch(cargarProductosPorFiltroSeleccionado({terminoBuscar: terminoBuscar,page: this.page, size: this.rows, disponibilidad: this.disponibilidad}));
  }

  onEnter(event:KeyboardEvent){
    if(event.key === 'Enter'){
      this.store.dispatch(cargarProductosPorFiltroSeleccionado({terminoBuscar: this.selectedValue,page: this.page, size: this.rows, disponibilidad: this.disponibilidad}));
    }
  }
}
