import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subject, takeUntil } from 'rxjs';
import { Store } from '@ngrx/store';

import { actualizarProducto, cargarProducto, crearProducto, redireccionarProductos, vaciarProducto } from '../../store/actions';
import { AppState } from '../../store/app.reducer';
import { errorProducto, loadingProducto } from '../../store/selectors/producto.selector';
import { Producto } from '../../interfaces/producto.interface';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styles: ``
})
export class ProductoComponent implements OnInit, OnDestroy{
  private store       : Store<AppState>;
  private router      : ActivatedRoute;
  private formBuilder :FormBuilder;
  public producto!    : Producto|null;
  public loading$!    : Observable<boolean>;
  public error$!      : Observable<any>;
  public formProducto : FormGroup;
  private destroy$    :Subject<void>;

  constructor(){
    this.destroy$     = new Subject<void>();
    this.formBuilder  = inject(FormBuilder);
    this.router       = inject(ActivatedRoute);
    this.store        = inject(Store);
    this.formProducto = this.formBuilder.group({
      id: [null],
      nombre: ['', [Validators.required, Validators.max(30)]],
      precio: [0, [Validators.required, Validators.min(0.99)]],
      url: ['']
    })
  }

  ngOnInit(): void {
      this.router.params.pipe(takeUntil(this.destroy$)).subscribe(({id})=>{
        if(!isNaN(id))
          this.store.dispatch(cargarProducto({id}));
        else
          this.store.dispatch(vaciarProducto())
      })

      this.loading$= this.store.select(loadingProducto).pipe(takeUntil(this.destroy$));
      this.error$= this.store.select(errorProducto).pipe(takeUntil(this.destroy$));

      this.store.select('producto').pipe(takeUntil(this.destroy$)).subscribe(data=>{
        if(data){
          this.producto=data.producto;
          this.formProducto.patchValue({
            id: this.producto?.id,
            nombre: this.producto?.nombre,
            precio: this.producto?.precio,
            url: this.producto?.url,
          })
        }
      })
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onSave():void{
    if(this.formProducto.invalid){
      this.formProducto.markAllAsTouched
      return;
    }
    if(this.formProducto.controls['id'].value){
      const producto: Producto={
        id:             this.formProducto.value.id as number,
        nombre:         this.formProducto.value.nombre as string,
        url:            this.formProducto.value.url as string,
        precio:         this.formProducto.value.precio as number,
        disponibilidad: true,
      }
      this.store.dispatch(actualizarProducto({producto:producto, id:this.formProducto.value.id as number}))
    }else{
      const producto: Producto={
        id:             undefined,
        nombre:         this.formProducto.value.nombre as string,
        url:            this.formProducto.value.url as string,
        precio:         this.formProducto.value.precio as number,
        disponibilidad: true,
      }
      this.store.dispatch(crearProducto({producto:producto}))
    }
    this.formProducto.reset();
    this.store.dispatch(redireccionarProductos({path:'/productos/listar'}));
  }
}
