import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Store } from '@ngrx/store';

import { actualizarCliente, cargarCliente, crearCliente, redirigir, vaciarCliente } from '../../store/actions';
import { AppStateWithClientes } from '../../store/reducers';
import { Cliente } from '../../interfaces/cliente.inteface';
import { errorCliente, loadingCliente } from '../../store/selectors/cliente.selector';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styles: ``
})
export class ClienteComponent implements OnInit,OnDestroy{
  private destroy$    : Subject<void>;
  private fb          : FormBuilder;
  private route       : ActivatedRoute;
  private store       : Store<AppStateWithClientes>;
  public formCliente  : FormGroup;
  public stateOptions : MenuItem[];
  public error$?      : Observable<any>;
  public loading$?    : Observable<boolean>;
  
  constructor(){
    this.destroy$ = new Subject<void>();
    this.fb       = inject(FormBuilder);
    this.route    = inject(ActivatedRoute);
    this.store    = inject(Store);

    this.stateOptions = [
      { label: 'Inactivo', value: false },
      { label: 'Activo', value: true }
    ];
    this.formCliente= this.fb.group({
      email  : ['', [Validators.email, Validators.required]],
      estado : [true, [Validators.required]],
      id     : [0],
      nit    : ['', [Validators.required, Validators.maxLength(20), Validators.minLength(7)]],
      nombre : ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
    })
  }

  ngOnInit(): void {
      this.route.params.pipe(takeUntil(this.destroy$)).subscribe(({id})=>{
        if (id)
          this.store.dispatch(cargarCliente({id: id}));
        else
          this.store.dispatch(vaciarCliente());
      });

      this.error$   = this.store.select(errorCliente).pipe(takeUntil(this.destroy$));
      this.loading$ = this.store.select(loadingCliente).pipe(takeUntil(this.destroy$));

      this.store.select('cliente').pipe(takeUntil(this.destroy$)).subscribe(data=>{
        if(data){
          this.formCliente.patchValue({
            email  : data.cliente?.email,
            estado : data.cliente?.activo,
            id     : data.id,
            nit    : data.cliente?.nit,
            nombre : data.cliente?.nombre,
          })
        }
      });
  }

  ngOnDestroy(): void { 
    this.destroy$.next();
    this.destroy$.complete();  
  }

  onSave():void{
    if(this.formCliente.invalid){
      this.formCliente.markAllAsTouched
      return;
    }

    const cliente:Cliente= {
      activo : this.formCliente.value.estado as boolean,
      email  : this.formCliente.value.email as string,
      id     : undefined,
      nit    : this.formCliente.value.nit as string,
      nombre : this.formCliente.value.nombre as string,
    }

    if(this.formCliente.controls['id'].value){
      cliente.id= this.formCliente.value.id as number,
      this.store.dispatch(actualizarCliente({cliente, id: this.formCliente.controls['id'].value}));
    }else{
      this.store.dispatch(crearCliente({cliente}));
    }
    this.formCliente.reset();
    this.store.dispatch(redirigir({path:'/clientes/listar'}));
  }
}
