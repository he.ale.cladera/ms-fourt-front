import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClienteRoutingModule } from './cliente-routing.module';
import { ListarClientesComponent } from './listar-clientes/listar-clientes.component';
import { ClienteComponent } from './cliente/cliente.component';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { clienteReducer, clientesReducer } from '../store/reducers';
import { LayoutComponent } from './layout/layout.component';
import { PrimengModule } from '../primeng/primeng.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    ListarClientesComponent,
    ClienteComponent,
    LayoutComponent,
  ],
  imports: [
    CommonModule,
    ClienteRoutingModule,
    RouterModule,
    StoreModule.forFeature('clientes', clientesReducer),
    StoreModule.forFeature( 'cliente', clienteReducer),
    PrimengModule,
    ReactiveFormsModule,
    SharedModule,
  ]
})
export class ClienteModule { }
