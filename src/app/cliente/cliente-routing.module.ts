import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListarClientesComponent } from './listar-clientes/listar-clientes.component';
import { ClienteComponent } from './cliente/cliente.component';
import { LayoutComponent } from './layout/layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children:[
      {
        path: 'listar',
        component: ListarClientesComponent
      },
      {
        path: 'cliente/:id',
        component: ClienteComponent,
      },
      {
        path: 'crear',
        component: ClienteComponent,
      },
      {
        path: '**',
        redirectTo: 'listar',
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClienteRoutingModule { }
