import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, inject } from '@angular/core';

import { Observable, Subject, take, takeUntil } from 'rxjs';

import { Store } from '@ngrx/store';

import { MenuItem } from 'primeng/api';

import { activarDesactivarCliente, cargarClientes, cargarClientesPorFiltroSeleccionado, cargarFiltroClientes } from '../../store/actions';
import { AppStateWithClientes } from '../../store/reducers';
import { loadingClientes, mostrarError, selcecionarClientes, seleccionarFiltrosClientes } from '../../store/selectors/clientes.selector';

import { Cliente } from '../../interfaces/cliente.inteface';
import { PaginatorState } from 'primeng/paginator';
import { AutoCompleteCompleteEvent, AutoCompleteSelectEvent } from 'primeng/autocomplete';

@Component({
  selector: 'app-listar-clientes',
  templateUrl: './listar-clientes.component.html',
  styles: [``],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListarClientesComponent implements OnInit, OnDestroy{
  private destroy$     : Subject<void>;
  public clientes$?    : Observable<Cliente[]>;
  public error$?       : Observable<any>;
  public estado        : boolean;
  public items         : MenuItem[];
  public loading$?     : Observable<boolean>;
  public page          : number;
  public rows          : number;
  public selectedValue!: string;
  public suggestions$? : Observable<string[]>;
  public totalElements : number;

  private store       : Store<AppStateWithClientes>
  constructor( ){
    this.estado        = true;
    this.page          = 0;
    this.rows          = 10;
    this.totalElements = 0;
    this.store         = inject(Store);
    this.destroy$      = new Subject<void>();
    this.items         = [
      {
        label: 'Opciones',
        icon: 'pi pi-fw',
        items: [
          { label: 'Activo', icon:'pi pi-check',command: () => this.handleItemClick(true)},
          { label: 'Inactivo',icon:'pi pi-ban', command: () => this.handleItemClick(false)},
        ]
      }
    ]
  }

  ngOnInit(): void {
      this.store.select('clientes').pipe(takeUntil(this.destroy$)).subscribe(data=>{
        this.page= data.page;
        this.rows= data.size;
        this.totalElements= data.totalElements;
        this.estado= data.estado;
      });
      this.store.select(seleccionarFiltrosClientes).pipe(takeUntil(this.destroy$));
      this.clientes$    = this.store.select(selcecionarClientes).pipe(takeUntil(this.destroy$));
      this.loading$     = this.store.select(loadingClientes).pipe(takeUntil(this.destroy$));
      this.error$       = this.store.select(mostrarError).pipe(takeUntil(this.destroy$));
      this.suggestions$ = this.store.select(seleccionarFiltrosClientes).pipe(takeUntil(this.destroy$));
      this.store.dispatch(cargarClientes({page: this.page, size: this.rows, estado: this.estado}));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onPageChange(event: PaginatorState): void{
    this.clientes$?.pipe(take(1)).subscribe(data=>console.log(data))
    console.log(event)
    if (event.page && event.rows){
      this.store.dispatch(cargarClientes({page: event.page, size: event.rows, estado: this.estado}));
    }
  }

  handleItemClick(event: boolean): void {
    if (event){
      this.store.dispatch(cargarClientes({page: 0, size: this.rows , estado: event}));
    }else{
      this.store.dispatch(cargarClientes({page: 0, size: this.rows , estado: event}));
    }
  }

  onDesactivarCliente(id: number){
    this.store.dispatch(activarDesactivarCliente({idCliente:id, page: this.page, size: this.rows, estado: this.estado}));
  }

  search(event: AutoCompleteCompleteEvent) {
    const query = event.query;
    if(query.length>=3 && query){
      this.store.dispatch(cargarFiltroClientes({terminoBuscar:query}));
    }
  }

  onSelect(event: AutoCompleteSelectEvent){
    const filtro= event.value;
    this.store.dispatch(cargarClientesPorFiltroSeleccionado({terminoBuscar: filtro,page: this.page, size: this.rows, estado: this.estado}));
  }

  onEnter(event:KeyboardEvent){
    if (event.key === 'Enter') {
      this.store.dispatch(cargarClientesPorFiltroSeleccionado({terminoBuscar: this.selectedValue,page: this.page, size: this.rows, estado: this.estado}));
    }
  }
}
