import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'productos',
    loadChildren: ()=>import('./producto/producto.module').then(m=>m.ProductoModule),
  },
  {
    path: 'clientes',
    loadChildren: ()=>import('./cliente/cliente.module').then(m=>m.ClienteModule),
  },
  {
    path: 'facturas',
    loadChildren: ()=>import('./factura/factura.module').then(m=>m.FacturaModule),
  },
  {
    path: '**',
    redirectTo: 'productos'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
