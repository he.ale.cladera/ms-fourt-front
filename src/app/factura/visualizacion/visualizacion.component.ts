import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, take, takeUntil } from 'rxjs';
import { FacturaService } from '../../services/factura.service';

@Component({
  selector: 'app-visualizacion',
  templateUrl: './visualizacion.component.html',
  styleUrl: './visualizacion.component.css'
})
export class VisualizacionComponent implements OnInit, OnDestroy{
  id:number=0;
  private destroy$ = new Subject<void>();

  constructor(private route: ActivatedRoute, private facturaService: FacturaService){

  }

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy$)).subscribe(({ id }) => {
      this.id=id;
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  descargarReporte() {
    this.facturaService.generarReporte(this.id).pipe(take(1)).subscribe(response => {
      const blob = new Blob([response], { type: 'application/pdf' });
        const url = window.URL.createObjectURL(blob);
        window.open(url);
    });
  }
}
