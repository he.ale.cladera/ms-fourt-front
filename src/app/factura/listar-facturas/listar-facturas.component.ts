import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, inject } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable, Subject, takeUntil } from 'rxjs';

import { AppStateWithFacturas } from '../../store/reducers/facturas.reducer';
import { cargarFacturas } from '../../store/actions';
import { Factura } from '../../interfaces/factura.interface';
import { loadingFacturas, mostrarErrorFacturas, selcecionarFacturas } from '../../store/selectors';

@Component({
  selector: 'app-listar-facturas',
  templateUrl: './listar-facturas.component.html',
  styles: ``,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListarFacturasComponent implements OnInit, OnDestroy{
  private store        : Store<AppStateWithFacturas>;
  public destroy$      : Subject<void>;
  public error$!       : Observable<any>;
  public facturas$!    : Observable<Factura[]>;
  public loading$!     : Observable<boolean>;
  public page          : number;
  public rows          : number;
  public totalElements!: number;

  constructor(){
    this.destroy$= new Subject<void>();
    this.page    = 0;
    this.rows    = 10;
    this.store   = inject(Store);
  }

  ngOnInit(): void {
    this.store.select('facturas').subscribe(data=>{
      this.page= data.page;
      this.rows= data.size;
      this.totalElements= data.totalElements;
    });
    this.error$   = this.store.select(mostrarErrorFacturas).pipe(takeUntil(this.destroy$));
    this.facturas$= this.store.select(selcecionarFacturas).pipe(takeUntil(this.destroy$));
    this.loading$ = this.store.select(loadingFacturas).pipe(takeUntil(this.destroy$));
    this.store.dispatch(cargarFacturas({page: this.page, size: this.rows}))
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  eliminar(id:any):void{
    this.store.dispatch(cargarFacturas({page: this.page, size: this.rows}))
  }

  onPageChange(event: any){
    this.store.dispatch(cargarFacturas({page: event.page, size: event.rows}))
  }
}
