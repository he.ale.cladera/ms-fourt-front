import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FacturaRoutingModule } from './factura-routing.module';
import { ListarFacturasComponent } from './listar-facturas/listar-facturas.component';
import { FacturaComponent } from './factura/factura.component';
import { LayoutComponent } from './layout/layout.component';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { facturaReducer } from '../store/reducers/factura.reducer';
import { facturasReducer } from '../store/reducers/facturas.reducer';
import { PrimengModule } from '../primeng/primeng.module';
import { ReactiveFormsModule } from '@angular/forms';
import { VisualizacionComponent } from './visualizacion/visualizacion.component';

import { PdfViewerModule } from 'ng2-pdf-viewer';
import { SharedModule } from '../shared/shared.module';
import { clienteReducer, clientesReducer, productoReducer, productosReducer } from '../store/reducers';

@NgModule({
  declarations: [
    ListarFacturasComponent,
    FacturaComponent,
    LayoutComponent,
    VisualizacionComponent
  ],
  imports: [
    CommonModule,
    FacturaRoutingModule,
    RouterModule,
    StoreModule.forFeature('factura', facturaReducer),
    StoreModule.forFeature('facturas', facturasReducer),
    StoreModule.forFeature('clientes', clientesReducer),
    StoreModule.forFeature('cliente', clienteReducer),
    StoreModule.forFeature('productos', productosReducer),
    StoreModule.forFeature('producto', productoReducer),
    PrimengModule,
    ReactiveFormsModule,
    PdfViewerModule,
    SharedModule,
  ]
})
export class FacturaModule { }
