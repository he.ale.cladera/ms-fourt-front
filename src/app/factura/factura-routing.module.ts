import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { ListarFacturasComponent } from './listar-facturas/listar-facturas.component';
import { FacturaComponent } from './factura/factura.component';
import { VisualizacionComponent } from './visualizacion/visualizacion.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children:[
      {
        path: 'listar',
        component: ListarFacturasComponent,
      },
      {
        path: 'factura/:id',
        component: FacturaComponent,
      },
      {
        path: 'crear',
        component: FacturaComponent,
      },
      {
        path: 'visualizar/:id',
        component: VisualizacionComponent,
      },
      {
        path: '**',
        redirectTo: 'listar',
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FacturaRoutingModule { }
