import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, Subject, filter, take, takeUntil } from 'rxjs';
import { AppStateWithFacturas } from '../../store/reducers';
import { loadingFactura, mostrarErrorFactura, mostrarFactura, obtenerProducto, seleccionarFiltrosClientes, seleccionarFiltrosProductos } from '../../store/selectors';
import { FacturaDetallada } from '../../interfaces/FacturaDetallada.inteface';
import { actualizarFactura, buscarProductoByName, cargarFactura, cargarFiltroClientes, cargarFiltroProductos, cargarProductosPorFiltroSeleccionado, crearFactura, redirigirVisualizar, vaciarFactura, vaciarProducto } from '../../store/actions';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AutoCompleteCompleteEvent, AutoCompleteSelectEvent } from 'primeng/autocomplete';
import { Producto } from '../../interfaces/producto.interface';
import { Factura } from '../../interfaces/factura.interface';

@Component({
  selector: 'app-factura',
  templateUrl: './factura.component.html',
  styleUrl: './factura.component.css',
})
export class FacturaComponent implements OnInit, OnDestroy {
  private destroy$     : Subject<void>;
  private fb           : FormBuilder;
  private route        : ActivatedRoute;
  private store        : Store<AppStateWithFacturas>;
  public error$!       : Observable<any>;
  public factura!      : FacturaDetallada|null;
  public facturaForm   : FormGroup;
  public loading$!     : Observable<boolean>;
  public nitsClientes$!: Observable<string[]>;
  public producto$!    : Observable<Producto|null>;
  public productos$!   : Observable<string[]>;

  constructor(){
    this.destroy$    = new Subject();
    this.route       = inject(ActivatedRoute);
    this.store       = inject(Store);
    this.fb          = inject(FormBuilder);
    this.facturaForm = this.fb.group({
      id: [0],
      fecha: ['', [Validators.required]],
      total: [0, [Validators.required]],
      nitCliente: ['', [Validators.required]],
      detalles: this.fb.array([]),
    })
  }

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy$)).subscribe(({id})=>{
      if(id)
        this.store.dispatch(cargarFactura({id: id}));
      else
        this.store.dispatch(vaciarFactura());
    });

    this.store.select(mostrarFactura).pipe(takeUntil(this.destroy$)).subscribe(data=>{
      if(data)
        this.cargarFormulario(data);
    });
    this.error$= this.store.select(mostrarErrorFactura).pipe(takeUntil(this.destroy$));
    this.loading$= this.store.select(loadingFactura).pipe(takeUntil(this.destroy$));
    this.nitsClientes$= this.store.select(seleccionarFiltrosClientes).pipe(takeUntil(this.destroy$));
    this.producto$= this.store.select(obtenerProducto).pipe(takeUntil(this.destroy$));
    this.productos$= this.store.select(seleccionarFiltrosProductos).pipe(takeUntil(this.destroy$));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  get detalles(): FormArray {
    return this.facturaForm.get('detalles') as FormArray;
  }

  cargarFormulario(fact: FacturaDetallada):void{
    this.factura={...fact};
    this.facturaForm.patchValue({
      id        : fact?.id,
      nitCliente: fact?.cliente?.nit,
      fecha     : fact?.fecha,
      total     : fact?.total,
    });

    if (this.factura?.detalles) {
      this.factura?.detalles.forEach((e) => {
        const elemento = this.fb.group({
          id            : [e.id],
          productoNombre: [e.productoNombre, Validators.required],
          cantidad      : [e.cantidad, [Validators.required, Validators.min(1)]],
          precioUnitario: [e.precioUnitario, [Validators.required, Validators.min(0)]],
          subtotal      : [e.subtotal, Validators.required]
        });
        this.detalles.push(elemento);
      });
    }
  }

  /**Cliente */
  filterCliente(event:AutoCompleteCompleteEvent):void{
    const query=event.query;
    if(query.length>=3)
      this.store.dispatch(cargarFiltroClientes({terminoBuscar:query}))
  }

  /**Detalles */
  filterProducto(event:AutoCompleteCompleteEvent):void{
    const query=event.query;
    if(query.length>=3)
      this.store.dispatch(cargarFiltroProductos({terminoBuscar:query}));
  }

  cargarProducto(event: AutoCompleteSelectEvent, index: number):void{
    const nombreProducto= event.value;
    this.store.dispatch(buscarProductoByName({nombreProducto: nombreProducto}));
    this.producto$.pipe(
      filter(data => !!data), // Asegúrate de que data no sea null o undefined
      take(1)
    ).subscribe(data => {
      console.log(index)
      const detalle = this.detalles.at(index);
      detalle.get('precioUnitario')?.setValue(data!.precio, { emitEvent: false });
      this.calculateSubtotal(index);
      this.calculateTotal();
      this.store.dispatch(vaciarProducto());
    });
  }
  
  calculateSubtotal(index:number):void{
    const detalle = this.detalles.at(index);
    const cantidad = detalle.get('cantidad')?.value;
    const precioUnitario = detalle.get('precioUnitario')?.value;
    const subtotal = cantidad * precioUnitario;
    detalle.get('subtotal')?.setValue(subtotal, { emitEvent: false });
    this.calculateTotal();
  }

  calculateTotal(): void {
    let total = 0;
    this.detalles.controls.forEach(detalle => {
      total += detalle.get('subtotal')?.value || 0;
    });
    this.facturaForm.get('total')?.setValue(total, { emitEvent: false });
  }
  
  addDetalle(): void {
    const detalleFormGroup = this.fb.group({
      id            : [0],
      productoNombre: ['', Validators.required],
      cantidad      : [1, [Validators.required, Validators.min(1)]],
      precioUnitario: [0, [Validators.required, Validators.min(0)]],
      subtotal      : [0]
    });
    this.detalles.push(detalleFormGroup);
  }

  removeDetalle(index: number): void {
    this.detalles.removeAt(index);
  }

  submitForm(){
    if(this.facturaForm.invalid){
      this.facturaForm.markAllAsTouched
      return;
    }
    const facturaFormulario: Factura = this.facturaForm.value;
    if(this.facturaForm.controls['id'].value){
      this.store.dispatch(actualizarFactura({factura: facturaFormulario}));
    }else{
      this.store.dispatch(crearFactura({factura:facturaFormulario}));
    }
    this.store.dispatch(redirigirVisualizar({id: facturaFormulario.id}));
  }
}
