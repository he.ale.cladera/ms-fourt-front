export interface ProductoPage {
    content:          Producto[];
    pageable:         Pageable;
    last:             boolean;
    totalPages:       number;
    totalElements:    number;
    size:             number;
    number:           number;
    sort:             any[];
    first:            boolean;
    numberOfElements: number;
    empty:            boolean;
}

export interface Producto {
    id:             number|undefined;
    nombre:         string;
    precio:         number;
    url:            string;
    disponibilidad: boolean;

}

export interface Pageable {
    pageNumber: number;
    pageSize:   number;
    sort:       any[];
    offset:     number;
    unpaged:    boolean;
    paged:      boolean;
}
