import { Cliente } from "./cliente.inteface";

export interface FacturaDetallada {
    id:       number;
    fecha:    Date;
    total:    number;
    cliente:  Cliente;
    detalles: Detalle[];
}

export interface Detalle {
    id:             number;
    facturaId:      number;
    productoNombre: string;
    cantidad:       number;
    precioUnitario: number;
    subtotal:       number;
}
