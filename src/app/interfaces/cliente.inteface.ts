export interface PageCliente {
    content:          Cliente[];
    pageable:         Pageable;
    last:             boolean;
    totalElements:    number;
    totalPages:       number;
    size:             number;
    number:           number;
    sort:             any[];
    numberOfElements: number;
    first:            boolean;
    empty:            boolean;
}

export interface Cliente {
    id:     number|undefined;
    nombre: string;
    email:  string;
    nit:    string;
    activo: boolean;
}

export interface Pageable {
    pageNumber: number;
    pageSize:   number;
    sort:       any[];
    offset:     number;
    unpaged:    boolean;
    paged:      boolean;
}
