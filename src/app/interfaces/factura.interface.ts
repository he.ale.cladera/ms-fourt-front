export interface PageFactura {
    content:          Factura[];
    pageable:         Pageable;
    last:             boolean;
    totalPages:       number;
    totalElements:    number;
    size:             number;
    number:           number;
    sort:             any[];
    numberOfElements: number;
    first:            boolean;
    empty:            boolean;
}

export interface Factura {
    id:         number;
    fecha:      Date;
    total:      number;
    nitCliente: string;
}

export interface Pageable {
    pageNumber: number;
    pageSize:   number;
    sort:       any[];
    offset:     number;
    paged:      boolean;
    unpaged:    boolean;
}
