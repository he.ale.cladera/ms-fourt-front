export interface ErrorBackend {
    payload: Payload;
}

export interface Payload {
    headers:    Headers;
    status:     number;
    statusText: string;
    url:        string;
    ok:         boolean;
    name:       string;
    message:    string;
    error:      Error;
}

export interface Error {
    path:      string;
    error:     string;
    message:   string;
    timestamp: Date;
    status:    number;
}

export interface Headers {
    normalizedNames: NormalizedNames;
    lazyUpdate:      null;
}

export interface NormalizedNames {
}
