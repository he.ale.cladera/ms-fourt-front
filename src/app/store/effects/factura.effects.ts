import { Injectable, inject } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { actualizarFactura, cargarFactura, cargarFacturaError, cargarFacturaSuccess, crearFactura, redirigirVisualizar, successOperation, vaciarFactura } from "../actions";
import { catchError, map, mergeMap, of, tap } from "rxjs";
import { FacturaService } from "../../services/factura.service";
import { Router } from "@angular/router";

@Injectable()
export class FacturaEffects{
    actions$= inject(Actions);

    constructor(private facturaService:FacturaService, 
                private router:Router){}

    cargarFactura$= createEffect(
        ()=>this.actions$.pipe(
            ofType(cargarFactura),
            mergeMap(
                (action:any)=>{
                    if(!action.id){
                        return of(vaciarFactura);
                    }
                    return this.facturaService.obtenerFactura(action.id).pipe(
                        map(data=>cargarFacturaSuccess({factura:data})),
                        catchError(err=>of(cargarFacturaError({payload:err})))
                    )
                }
            )
        )
    )

    crearFactura$=createEffect(
        ()=>this.actions$.pipe(
            ofType(crearFactura),
            mergeMap(
                (action:any)=>
                    this.facturaService.crearFactura(action.factura).pipe(
                        map(data=>{
                                this.router.navigate(['/facturas/visualizar', data.id])
                                return successOperation({id:data.id})
                            }
                        ),
                        catchError(err=>of(cargarFacturaError({payload: err})))
                    )
            )
        )
    )

    actualizarFactura$=createEffect(
        ()=>this.actions$.pipe(
            ofType(actualizarFactura),
            mergeMap(
                (action:any)=>
                    this.facturaService.actualizarFactura(action.factura.id,action.factura).pipe(
                        map(data=>successOperation({id:data.id})),
                        catchError(err=>of(cargarFacturaError({payload: err})))
                    )
            )
        )
    )

    redirigirVisualizarFactura$ = createEffect(() =>
        this.actions$.pipe(
            ofType(redirigirVisualizar),
            tap((action) => this.router.navigate(['/facturas/visualizar', action.id]))
        ),
        { dispatch: false }
    );
}