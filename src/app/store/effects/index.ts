import { ClienteEffect } from "./cliente.effects";
import { ClientesEffect } from "./clientes.effects";
import { FacturaEffects } from "./factura.effects";
import { FacturasEffects } from "./facturas.effects";
import { ProductoEffect } from "./producto.effects";
import { ProductosEffects } from "./productos.effects";

export const EffectsArray: any[]= [ProductosEffects, ProductoEffect, ClientesEffect, ClienteEffect, FacturasEffects, FacturaEffects];