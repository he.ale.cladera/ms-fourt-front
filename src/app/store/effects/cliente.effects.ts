import { Injectable, inject } from "@angular/core";
import { Actions, act, createEffect, ofType } from "@ngrx/effects";
import { ClienteService } from "../../services/cliente.service";
import { actualizarCliente, cargarCliente, cargarClienteError, cargarClienteSuccess, crearCliente, redirigir, vaciarCliente } from "../actions";
import { catchError, map, mergeMap, of, tap } from "rxjs";
import { Router } from "@angular/router";
import { ErrorBackend, Payload } from "../../interfaces/error.interface";

@Injectable()
export class ClienteEffect{
    actions$= inject(Actions)
    router= inject(Router);

    constructor(private clienteService: ClienteService){}

    cargarCliente$= createEffect(
        ()=>this.actions$.pipe(
            ofType(cargarCliente),
            mergeMap(
                (action: any)=>{
                    if(!action.id){
                        return of(vaciarCliente())
                    }
                    return this.clienteService.obtenerCliente(action.id).pipe(
                        map(data=>cargarClienteSuccess({cliente: data})),
                        catchError((err:ErrorBackend)=>of(cargarClienteError({payload:err})))
                    );
                }
            )
        )
    )

    guardarCliente$=createEffect(
        ()=>this.actions$.pipe(
            ofType(crearCliente),
            mergeMap(
                (action:any)=>
                    this.clienteService.crearCliente(action.cliente).pipe(
                        map(data=>cargarClienteSuccess({cliente: data})),
                        catchError(err=>of(cargarClienteError({payload:err})))
                    )
            )
        )
    )

    actualizarCliente$=createEffect(
        ()=>this.actions$.pipe(
            ofType(actualizarCliente),
            mergeMap(
                (action:any)=>
                    this.clienteService.guardarCliente(action.cliente, action.id).pipe(
                        map(data=>cargarClienteSuccess({cliente: data})),
                        catchError(err=>of(cargarClienteError({payload:err})))
                    )
            )
        )
    )

    redirigir$ = createEffect(() =>
        this.actions$.pipe(
            ofType(redirigir),
            tap((action) => this.router.navigate([action.path]))
        ),
        { dispatch: false }
    );
}