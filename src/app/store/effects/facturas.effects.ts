import { Injectable, inject } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { FacturaService } from "../../services/factura.service";
import { cargarFacturas, cargarFacturasError, cargarFacturasSuccess } from "../actions";
import { catchError, distinctUntilChanged, map, mergeMap, of } from "rxjs";

@Injectable()
export class FacturasEffects{
    action$= inject(Actions);
    constructor(private facturaService: FacturaService){}

    cargarFacturas$= createEffect(
        ()=>this.action$.pipe(
            ofType(cargarFacturas),
            distinctUntilChanged((prev, curr) => JSON.stringify(prev) === JSON.stringify(curr)),
            mergeMap(
                (action: any)=>this.facturaService.obtenerFacturas(action.page, action.size).pipe(
                    map(data=> cargarFacturasSuccess({facturas:data.content, totalElements: data.totalElements})),
                    catchError(err=>of(cargarFacturasError(err))),
                )
            )
        )
    )
}