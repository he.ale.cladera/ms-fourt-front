import { Injectable, inject } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";

import { ProductoService } from "../../services/producto.service";
import { cargarFiltroProductos, cargarFiltroProductosError, cargarFiltroProductosSuccess, cargarProductos, cargarProductosError, cargarProductosPorFiltroError, cargarProductosPorFiltroSeleccionado, cargarProductosSuccess, disponibilidadProducto } from "../actions";
import { catchError, map, mergeMap, of } from "rxjs";

@Injectable()
export class ProductosEffects{
    private action$= inject(Actions)

    constructor(private productoService: ProductoService){}

    cargarProductos$: any = createEffect(
        ()=>this.action$.pipe(
            ofType(cargarProductos),
            mergeMap(
                (action: any)=>this.productoService.obtenerProductos(action.page, action.size, action.disponibilidad).pipe(
                    map(data=> cargarProductosSuccess({productos: data.content, totalElements: data.totalElements})),
                    catchError(err=>of(cargarProductosError({payload: err}))),
                )
            )
        )
    )

    disponibildadProducto$= createEffect(
        ()=>this.action$.pipe(
            ofType(disponibilidadProducto),
            mergeMap(
                (action: any)=>this.productoService.setDisponibilidadYObtenerProductos(action.idProducto, action.page, action.size, action.estado)
                .pipe(
                    map(data=>cargarProductosSuccess({productos:data.content, totalElements: data.totalElements })),
                    catchError(err=>of(cargarProductosError({payload: err})))
                )
            )
        )
    )

    fitrosProductos$= createEffect(
        ()=>this.action$.pipe(
            ofType(cargarFiltroProductos),
            mergeMap(
                (action: any)=>this.productoService.findProductoByNombre(action.terminoBuscar).pipe(
                    map(data=>cargarFiltroProductosSuccess({filtros: data})),
                    catchError(err=>of(cargarFiltroProductosError({error:err})))
                )
            )
        )
    )
    
    cargarProductosFiltroSelecionado$= createEffect(
        ()=>this.action$.pipe(
            ofType(cargarProductosPorFiltroSeleccionado),
            mergeMap(
                (action: any)=> this.productoService.obtenerProductosName(action.terminoBuscar,action.page, action.size, action.disponibilidad).pipe(
                    map(data=>cargarProductosSuccess({productos: data.content, totalElements: data.totalElements})),
                    catchError(err=>of(cargarProductosPorFiltroError({payload:err})))
                )
            )
        )
    )
}