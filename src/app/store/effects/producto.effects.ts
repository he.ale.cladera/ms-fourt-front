import { Injectable, inject } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { ProductoService } from "../../services/producto.service";
import { actualizarProducto, buscarProductoByName, cargarProducto, cargarProductoError, cargarProductoSuccess, crearProducto, redireccionarProductos, vaciarProducto } from "../actions";
import { catchError, map, mergeMap, of, tap } from "rxjs";
import { Router } from "@angular/router";

@Injectable()
export class ProductoEffect{
    private actions$= inject(Actions);
    private router: Router= inject(Router);

    constructor(private productoService: ProductoService){}

    cargarProducto$: any= createEffect(
        ()=>this.actions$.pipe(
            ofType(cargarProducto),
            mergeMap(
                (action: any)=>{ 
                    if(!action.id)
                        return of(vaciarProducto())
                    return this.productoService.obtenerProducto(action.id).pipe(
                    map(data=> cargarProductoSuccess({producto: data})),
                    catchError(err=> of(cargarProductoError({payload: err})))
                    )
                }
            )
        )
    );

    crearProducto$= createEffect(
        ()=>this.actions$.pipe(
            ofType(crearProducto),
            mergeMap(
                (action: any)=>
                    this.productoService.crearProducto(action.producto).pipe(
                        map(data=>cargarProductoSuccess({producto:data})),
                        catchError(err=>of(cargarProductoError({payload:err})))
                    )
            )
        )
    );
    actualizarProducto$= createEffect(
        ()=>this.actions$.pipe(
            ofType(actualizarProducto),
            mergeMap(
                (action: any)=>
                    this.productoService.actualizarProducto(action.producto, action.id).pipe(
                        map(data=>cargarProductoSuccess({producto:data})),
                        catchError(err=>of(cargarProductoError({payload:err})))
                    )
            )
        )
    );

    regresarProductos$=createEffect(
        ()=>this.actions$.pipe(
            ofType(redireccionarProductos),
            tap((action) => this.router.navigate([action.path]))
        ),
        { dispatch: false }
    );

    cargarProductoByName$: any= createEffect(
        ()=>this.actions$.pipe(
            ofType(buscarProductoByName),
            mergeMap(
                (action: any)=>this.productoService.getProductFactura(action.nombreProducto).pipe(
                    map(data=> cargarProductoSuccess({producto: data})),
                    catchError(err=> of(cargarProductoError({payload: err})))
                    )
            )
        )
    );
}