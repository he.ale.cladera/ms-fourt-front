import { Injectable, inject } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { ClienteService } from "../../services/cliente.service";
import { activarDesactivarCliente, cargarClientes, cargarClientesError, cargarClientesPorFiltroError, cargarClientesPorFiltroSeleccionado, cargarClientesSuccess, cargarFiltroClientes, cargarFiltroClientesError, cargarFiltroClientesSuccess } from "../actions";
import { catchError,  map, mergeMap, of,  } from "rxjs";

@Injectable()
export class ClientesEffect{
    actions$= inject(Actions)

    constructor(private clienteService: ClienteService){}

    cargarClientes$= createEffect(
        ()=>this.actions$.pipe(
            ofType(cargarClientes),
            mergeMap(
                (action: any)=>this.clienteService.obtenerClientes(action.page, action.size, action.estado).pipe(
                    map(data=>cargarClientesSuccess({clientes: data.content, totalElements: data.totalElements})),
                    catchError(err=>of(cargarClientesError({payload:err})))
                )
            )
        )
    )

    desactivarCliente$= createEffect(
        ()=>this.actions$.pipe(
            ofType(activarDesactivarCliente),
            mergeMap(
                (action:any)=>this.clienteService.desacctivarActualizarClientes(action.idCliente, action.page, action.size, action.estado).pipe(
                    map(data=>{
                        return cargarClientesSuccess({clientes: data.content, totalElements: data.totalElements})}),
                    catchError(err=>of(cargarClientesError({payload:err})))
                )

            )

        )
    )

    fitrosCliente$= createEffect(
        ()=>this.actions$.pipe(
            ofType(cargarFiltroClientes),
            mergeMap(
                (action: any)=>this.clienteService.findClientesByIdOrNit(action.terminoBuscar).pipe(
                    map(data=>cargarFiltroClientesSuccess({filtros: data})),
                    catchError(err=>of(cargarFiltroClientesError({error:err})))
                )
            )
        )
    )
    
    cargarClientesFiltroSelecionado$= createEffect(
        ()=>this.actions$.pipe(
            ofType(cargarClientesPorFiltroSeleccionado),
            mergeMap(
                (action: any)=>{
                    console.log(action)
                    return this.clienteService.obtenerClientesNitOrName(action.terminoBuscar,action.page, action.size, action.estado).pipe(
                    map(data=>cargarClientesSuccess({clientes: data.content, totalElements: data.totalElements})),
                    catchError(err=>of(cargarClientesPorFiltroError({payload:err})))
                )}
            )
        )
    )
}