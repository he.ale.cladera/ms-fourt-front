import { createAction, props } from "@ngrx/store";
import { Producto } from "../../interfaces/producto.interface";

export const cargarProducto = createAction(
    '[PRODUCTO] cargar producto',
    props<{id: number}>(),
);

export const cargarProductoSuccess = createAction(
    '[PRODUCTO] cargar producto success',
    props<{producto: Producto}>()
);

export const cargarProductoError= createAction(
    '[PRODUCTO] cargar producto error',
    props<{payload: any}>(),
)

export const vaciarProducto= createAction(
    '[PRODUCTO] vaciarProducto'
)

export const crearProducto= createAction(
    '[PRODUCTO] crear producto',
    props<{producto: Producto}>(),
)
export const actualizarProducto= createAction(
    '[PRODUCTO] actualizar producto',
    props<{producto: Producto, id: number}>(),
)
export const redireccionarProductos= createAction(
    '[PRODUCTO] redireccionar productos',
    props<{path: string}>(),
)

export const buscarProductoByName=createAction(
    '[PRODUCTO] buscar producto by name',
    props<{nombreProducto:string}>(),
)