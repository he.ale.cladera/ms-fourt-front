import { createAction, props } from "@ngrx/store";
import { Cliente } from "../../interfaces/cliente.inteface";
import { ErrorBackend } from "../../interfaces/error.interface";

export const cargarClientes = createAction(
    '[CLIENTES] cargar clientes',
    props<{page: number, size: number, estado: boolean}>(),
);

export const cargarClientesSuccess = createAction(
    '[CLIENTES] cargar clientes success',
    props<{clientes: Cliente[], totalElements: number}>(),
);

export const cargarClientesError = createAction(
    '[CLIENTES] cargar clientes error',
    props<{payload: ErrorBackend}>(),
)

export const activarDesactivarCliente= createAction(
    '[CLIENTE] activar desactivar cliente',
    props<{idCliente: number, page: number, size: number, estado: boolean}>(),
)

export const cargarFiltroClientes = createAction(
    '[CLIENTES] cargar filtro clientes',
    props<{terminoBuscar:string}>(),
);
export const cargarFiltroClientesSuccess = createAction(
    '[CLIENTES] cargar filtro clientes success',
    props<{filtros:string[]}>(),
);
export const cargarFiltroClientesError = createAction(
    '[CLIENTES] cargar filtro clientes error',
    props<{error: ErrorBackend}>(),
);

export const cargarClientesPorFiltroSeleccionado = createAction(
    '[CLIENTES] cargar clientes por filtro seleccionado',
    props<{terminoBuscar:string ,page: number, size: number, estado: boolean}>(),
);

export const cargarClientesPorFiltroError = createAction(
    '[CLIENTES] cargar clientes por filtro error',
    props<{payload: ErrorBackend}>(),
)