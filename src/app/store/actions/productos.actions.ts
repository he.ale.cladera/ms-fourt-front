import { createAction, props } from "@ngrx/store";
import { Producto } from "../../interfaces/producto.interface";
import { ErrorBackend } from "../../interfaces/error.interface";

export const cargarProductos = createAction(
    '[PRODUCTOS] cargar productos',
    props<{page: number, size: number, disponibilidad: boolean}>(),
);

export const cargarProductosSuccess= createAction(
    '[PRODUCTOS] cargar productos success',
    props<{productos: Producto[], totalElements: number}>(),
)

export const cargarProductosError= createAction(
    '[PRODUCTOS] crear productos error',
    props<{payload: ErrorBackend}>(),
)

export const disponibilidadProducto= createAction(
    '[PRODUCTOS] crear productos',
    props<{idProducto: number, page: number, size: number, estado: boolean}>(),
)

export const cargarFiltroProductos = createAction(
    '[PRODUCTOS] cargar filtro productos',
    props<{terminoBuscar:string}>(),
);
export const cargarFiltroProductosSuccess = createAction(
    '[PRODUCTOS] cargar filtro productos success',
    props<{filtros:string[]}>(),
);
export const cargarFiltroProductosError = createAction(
    '[PRODUCTOS] cargar filtro producto error',
    props<{error: ErrorBackend}>(),
);

export const cargarProductosPorFiltroSeleccionado = createAction(
    '[PRODUCTOS] cargar productos por filtro seleccionado',
    props<{terminoBuscar:string ,page: number, size: number, disponibilidad: boolean}>(),
);

export const cargarProductosPorFiltroError = createAction(
    '[PRODUCTOS] cargar clientes por filtro error',
    props<{payload: ErrorBackend}>(),
)