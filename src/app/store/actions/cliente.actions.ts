import { createAction, props } from "@ngrx/store";
import { Cliente } from "../../interfaces/cliente.inteface";
import { ErrorBackend } from "../../interfaces/error.interface";

export const cargarCliente = createAction(
    '[CLIENTE] cargar cliente',
    props<{id: number}>(),
);

export const cargarClienteSuccess= createAction(
    '[CLIENTE] cargar cliente success',
    props<{cliente: Cliente}>(),
)

export const cargarClienteError= createAction(
    '[CLIENTE] cargar cliente error',
    props<{payload: ErrorBackend}>(),
)

export const vaciarCliente= createAction(
    '[Cliente] vaciar cliente'
)

export const crearCliente= createAction(
    '[Cliente] crear cliente',
    props<{cliente: Cliente}>(),
)

export const actualizarCliente= createAction(
    '[Cliente] actualizar cliente',
    props<{cliente: Cliente, id: number}>(),
)

export const redirigir= createAction(
    '[CLIENTE] redirigir',
    props<{path: string}>()
)