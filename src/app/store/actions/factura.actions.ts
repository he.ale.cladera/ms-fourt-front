import { createAction, props } from "@ngrx/store";
import { FacturaDetallada } from "../../interfaces/FacturaDetallada.inteface";
import { Factura } from "../../interfaces/factura.interface";
import { ErrorBackend } from "../../interfaces/error.interface";

export const cargarFactura = createAction(
    '[FACTURA] cargar factura',
    props<{id: number}>(),
);

export const cargarFacturaSuccess = createAction(
    '[FACTURA] factura actions success',
    props<{factura: FacturaDetallada}> ()
);

export const cargarFacturaError = createAction(
    '[FACTURA] cargar factura error',
    props<{payload: ErrorBackend}>(),
)

export const vaciarFactura= createAction(
    '[FACTURA] vaciar factura',
)

export const cargarClientesFactura= createAction(
    '[FACTURA] cargar clientes',
    props<{nombre: string}>(),
)

export const crearFactura= createAction(
    '[FACTURA] crear factura',
    props<{factura : Factura}>(),
)

export const actualizarFactura= createAction(
    '[FACTURA] actualizar factura',
    props<{factura : Factura}>(),
)

export const successOperation= createAction(
    '[FACTURA] success operation factura',
    props<{id: number}>(),
)

export const redirigirVisualizar= createAction(
    '[FACTURA] redirigir visualizar',
    props<{id:number}>()
)