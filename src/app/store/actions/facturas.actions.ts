import { createAction, props } from "@ngrx/store";
import { Factura } from "../../interfaces/factura.interface";

export const cargarFacturas = createAction(
    '[FACTURAS] cargar facturas',
    props<{page:number, size: number}>(),
);

export const cargarFacturasSuccess = createAction(
    '[FACTURAS] cargar facturas success',
    props<{facturas: Factura[], totalElements: number}>(),
);

export const cargarFacturasError = createAction(
    '[FACTURAS] cargar facturas error',
    props<{payload: any}>(),
)