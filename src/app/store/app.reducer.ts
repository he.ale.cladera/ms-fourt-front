import { ActionReducerMap } from '@ngrx/store';
import { ProductosState, productosReducer } from './reducers/productos.reducer';
import { ProductoState, productoReducer } from './reducers';

export interface AppState {
    productos: ProductosState,
    producto: ProductoState
};

export const appReducer: ActionReducerMap<AppState>={
    productos: productosReducer,
    producto: productoReducer
}

