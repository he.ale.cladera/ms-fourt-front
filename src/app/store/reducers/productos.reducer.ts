import { createReducer, on } from '@ngrx/store';
import { Producto } from '../../interfaces/producto.interface';
import { cargarFiltroProductos, cargarFiltroProductosError, cargarFiltroProductosSuccess, cargarProductos, cargarProductosError, cargarProductosPorFiltroError, cargarProductosPorFiltroSeleccionado, cargarProductosSuccess, disponibilidadProducto } from '../actions';
import { ErrorBackend } from '../../interfaces/error.interface';


export interface ProductosState {
    disponibilidad   : boolean,
    error            : ErrorBackend|null,
    errorBuscarFiltro: ErrorBackend|null;
    errorFiltros     : ErrorBackend|null;
    filtros          : string[],
    loaded           : boolean,
    loading          : boolean,
    page             : number,
    productos        : Producto[],
    size             : number,
    totalElements    : number,
};

const initialState: ProductosState = {
    disponibilidad   : true,
    error            : null,
    errorBuscarFiltro: null,
    errorFiltros     : null,
    filtros          : [],
    loaded           : false,
    loading          : false,
    page             : 0,
    productos        : [],
    size             : 10,
    totalElements    : 0,
};

export const productosReducer = createReducer(
    initialState,
    on(
        cargarProductos,
        (state, {page, size, disponibilidad}) => ({
            ...state, loading: true, 
            page: page, 
            size: size, 
            disponibilidad: disponibilidad
        }),
    ),
    on(
        cargarProductosSuccess,
        (state, {productos, totalElements})=>({
            ...state,
            productos: productos,
            totalElements: totalElements,
            loaded: true,
            loading: false,
        })
    ),
    on(
        cargarProductosError,
        (state, {payload})=>({
            ...state,
            payload: payload
        }),
    ),
    on(
        disponibilidadProducto,
        (state, {page, size, estado})=>({
            ...state,
            disponibilidad:estado,
            page,size,
        })
    ),
    on(
        cargarFiltroProductos,
        (state)=>({...state}),
    ),
    on(
        cargarFiltroProductosSuccess,
        (state,{filtros})=>({
            ...state,
            filtros,
        }),
    ),
    on(
        cargarFiltroProductosError,
        (state,{error})=>({
            ...state,
            errorFiltros: error,
        }),
    ),
    on(
        cargarProductosPorFiltroSeleccionado,
        (state,{ page, size})=>({
            ...state,
            loading: true,
            page,
            size,
            estado: true,
        })
    ),
    on(
        cargarProductosPorFiltroError,
        (state, {payload})=>({
            ...state,
            loaded: false,
            loading: false,
            errorBuscarFiltro: payload,
        })
    ),
);