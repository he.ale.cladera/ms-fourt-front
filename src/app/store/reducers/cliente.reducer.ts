import { createReducer, on } from '@ngrx/store';
import { Cliente } from '../../interfaces/cliente.inteface';
import { cargarCliente, cargarClienteError, cargarClienteSuccess, crearCliente, vaciarCliente } from '../actions';
import { Error, ErrorBackend } from '../../interfaces/error.interface';

export interface ClienteState {
    cliente: Cliente|null,
    loaded: boolean,
    loading: boolean,
    error: ErrorBackend|null,
    id: number,
};


const initialState: ClienteState = {
    cliente: null,
    loaded: false,
    loading: false,
    error: null,
    id: 0,
};

export const clienteReducer = createReducer(
    initialState,
    on(
        cargarCliente,
        (state, {id}) => ({
            ...state,
            loading: true,
            id: id,
        }),
    ),
    on(
        cargarClienteSuccess,
        (state, {cliente})=>({
            ...state,
            loading: false,
            loaded: true,
            cliente: cliente,
        })
    ), 
    on(
        cargarClienteError,
        (state, {payload})=>({
            ...state,
            loaded: false,
            loading: false,
            error: payload,
        })
    ),
    on(
        vaciarCliente,
        (state)=>({
            ...state,
            cliente: null,
            loaded: false,
            loading: false,
            error: null,
            id: 0,
        })
    ),
    on(
        crearCliente,
        (state, {cliente})=>({
            ...state,
            cliente: cliente,
            loading: true,
            loaded: false,
        }),
    )
);