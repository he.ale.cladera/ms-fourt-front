import { createReducer, on } from '@ngrx/store';
import { actualizarFactura, cargarFactura, cargarFacturaError, cargarFacturaSuccess, crearFactura, successOperation, vaciarFactura } from '../actions';
import { FacturaDetallada } from '../../interfaces/FacturaDetallada.inteface';
import { ErrorBackend } from '../../interfaces/error.interface';


export interface FacturaState {
    error          : ErrorBackend|null,
    factura        : FacturaDetallada|null,
    id             : number,
    loaded         : boolean,
    loading        : boolean,
};

const initialState: FacturaState = {
    error          : null,
    factura        : null,
    id             : 0,
    loaded         : false,
    loading        : false,
};

export const facturaReducer = createReducer(
    initialState,
    on(
        cargarFactura,
        (state, {id}) => ({
            ...state,
            loading: true, 
            id: id
        }),
    ),
    on(
        cargarFacturaSuccess,
        (state, {factura})=>({
            ...state, 
            factura: factura, 
            loading: false, 
            loaded: true
        })
    ),
    on(
        cargarFacturaError,
        (state, {payload})=>({
            ...state, 
            loading: false, 
            error: payload,
        }),
    ),
    on(
        vaciarFactura,
        (state)=>({...state, loading: false, loaded: false, id:0, factura: null}),
    ),
    on(
        successOperation,
        (state)=>({
            ...state,
            id: 0,
            loaded: true,
            loading: false,
            productos:[],
            nombreProducto: '',
            nit: "",
            nits: [],
        })
    ),
    on(
        actualizarFactura,
        (state)=>({
            ...state,
            loading:true,
            loaded:false,
        })
    ),
    on(
        crearFactura,
        (state)=>({
            ...state,
            loading:true,
            loaded:false,
        })
    )
);