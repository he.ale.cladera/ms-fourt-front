import { createReducer, on } from '@ngrx/store';
import { cargarFacturas, cargarFacturasError, cargarFacturasSuccess } from '../actions';
import { AppState } from '../app.reducer';
import { FacturaState } from './factura.reducer';
import { Factura } from '../../interfaces/factura.interface';
import { ClientesState } from './clientes.reducer';
import { ClienteState } from './cliente.reducer';

export interface FacturasState {
    facturas: Factura[],
    loaded: boolean,
    loading: boolean,
    size: number,
    page: number,
    totalElements: number,
    error: any,        
};

const initialState: FacturasState = {
    facturas: [],
    loaded: false,
    loading: false,
    size: 10,
    page: 0,
    totalElements: 0,
    error: null,
};

export interface AppStateWithFacturas extends AppState{
    facturas: FacturasState,
    factura : FacturaState,
    clientes: ClientesState,
    cliente : ClienteState,
}

export const facturasReducer = createReducer(
    initialState,
    on(
        cargarFacturas,
        (state, {page, size}) => ({...state, loading: false, page: page, size: size }),
    ),
    on(
        cargarFacturasSuccess,
        (state,{facturas, totalElements})=>({...state, facturas: facturas, totalElements: totalElements, loaded:true, loading: false}),
    ),
    on(
        cargarFacturasError,
        (state, {payload})=>({...state, loading: false, error: payload}),
    )
);