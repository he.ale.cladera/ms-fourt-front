import { createReducer, on } from '@ngrx/store';
import { Producto } from '../../interfaces/producto.interface';
import { buscarProductoByName, cargarProducto, cargarProductoError, cargarProductoSuccess, vaciarProducto } from '../actions';
import { AppState } from '../app.reducer';
import { ErrorBackend } from '../../interfaces/error.interface';

export interface ProductoState {
    producto: Producto|null,
    loaded: boolean,
    loading: boolean,
    error: ErrorBackend|null,
    id: number,    
};

const initialState: ProductoState = {
    producto: null,
    loaded: false,
    loading: false,
    error: null,
    id: 0,
};

export const productoReducer = createReducer(
    initialState,
    on(
        cargarProducto,
        (state, {id}) => ({...state, loading: true, id: id}),
    ),
    on(
        cargarProductoSuccess,
        (state, {producto})=>({
            ...state,
            producto: producto,
            loaded: true,
            loading: false
        })
    ),
    on(
        cargarProductoError,
        (state, {payload})=>({
            ...state, 
            loaded: false,
            loading: false,
            error:payload,
        })
    ),
    on(
        vaciarProducto,
        (state)=>({
            ...state,
            producto: null,
            loaded: false,
            loading: false,
            error: null,
            id: 0,
        }),
    ),
    on(
        buscarProductoByName,
        (state)=>({
            ...state
        }),
    ),
);