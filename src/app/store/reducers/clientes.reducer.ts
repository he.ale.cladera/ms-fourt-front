import { createReducer, on } from '@ngrx/store';
import { Cliente } from '../../interfaces/cliente.inteface';
import { activarDesactivarCliente, cargarClientes, cargarClientesError, cargarClientesPorFiltroError, cargarClientesPorFiltroSeleccionado, cargarClientesSuccess, cargarFiltroClientes, cargarFiltroClientesError, cargarFiltroClientesSuccess } from '../actions';
import { AppState } from '../app.reducer';
import { ClienteState } from './cliente.reducer';
import { ErrorBackend } from '../../interfaces/error.interface';

export interface ClientesState {
    clientes         : Cliente[],
    page             : number,
    size             : number,
    totalElements    : number;
    loaded           : boolean,
    loading          : boolean,
    estado           : boolean,
    error            : ErrorBackend|null, 
    filtros          : string[],  
    errorFiltros     : ErrorBackend|null, 
    errorBuscarFiltro: ErrorBackend|null,
};

export interface AppStateWithClientes extends AppState{
    clientes: ClientesState,
    cliente : ClienteState,
}

const initialState: ClientesState = {
    clientes         : [],
    page             : 0,
    size             : 10,
    totalElements    : 0,
    loaded           : false,
    loading          : false,
    estado           : true,
    error            : null,
    filtros          : [],
    errorFiltros     : null,
    errorBuscarFiltro: null,
};

export const clientesReducer = createReducer(
    initialState,
    on(
        cargarClientes,
        (state, {page, size, estado}) => ({...state, loading: true, page: page, size: size, estado}),
    ),
    on(
        cargarClientesSuccess,
        (state, {clientes, totalElements})=>({
            ...state,
            loading: false,
            loaded: true,
            clientes: clientes,
            totalElements: totalElements,
        })    
    ),
    on(
        cargarClientesError,
        (state, {payload})=>({
            ...state,
            loaded: false,
            loading: false,
            error:payload
        })
    ),
    on(
        activarDesactivarCliente,
        (state, {page, size, estado})=>({
            ...state,
            page,size,estado,
        })
    ),
    on(
        cargarFiltroClientes,
        (state)=>({...state}),
    ),
    on(
        cargarFiltroClientesSuccess,
        (state,{filtros})=>({
            ...state,
            filtros,
        }),
    ),
    on(
        cargarFiltroClientesError,
        (state,{error})=>({
            ...state,
            errorFiltros: error,
        }),
    ),
    on(
        cargarClientesPorFiltroSeleccionado,
        (state,{ page, size})=>({
            ...state,
            loading: true,
            page,
            size,
            estado: true,
        })
    ),
    on(
        cargarClientesPorFiltroError,
        (state, {payload})=>({
            ...state,
            loaded: false,
            loading: false,
            errorBuscarFiltro: payload,
        })
    ),
);