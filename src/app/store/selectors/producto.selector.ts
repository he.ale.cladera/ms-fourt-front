import { createFeatureSelector, createSelector } from "@ngrx/store";
import { ProductoState } from "../reducers";

export const selectProductoState= createFeatureSelector<ProductoState>('producto');

export const errorProducto= createSelector(
    selectProductoState,
    (state: ProductoState)=>state.error,
)

export const loadingProducto= createSelector(
    selectProductoState,
    (state: ProductoState)=>state.loading,
)

export const obtenerProducto= createSelector(
    selectProductoState,
    (state: ProductoState)=>state.producto,
)