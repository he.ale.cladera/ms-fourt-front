import { createFeatureSelector, createSelector } from "@ngrx/store";
import { ProductosState } from "../reducers";


export const selectProductosState= createFeatureSelector<ProductosState>('productos');

export const seleccionarProductos= createSelector(
    selectProductosState,
    (state:ProductosState)=>state.productos,
)

export const mostrarErrorProductos= createSelector(
    selectProductosState,
    (state: ProductosState)=>state.error,
)

export const loadingProductos= createSelector(
    selectProductosState,
    (state: ProductosState)=>state.loading,
)

export const seleccionarFiltrosProductos= createSelector(
    selectProductosState,
    (state: ProductosState)=>state.filtros,
)