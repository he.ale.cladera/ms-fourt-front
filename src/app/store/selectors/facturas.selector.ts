import { createFeatureSelector, createSelector } from "@ngrx/store"
import { FacturasState } from "../reducers"


export const selectFacturasState= createFeatureSelector<FacturasState>('facturas')

export const selcecionarFacturas= createSelector(
    selectFacturasState,
    (state: FacturasState)=> state.facturas,
)

export const mostrarErrorFacturas= createSelector(
    selectFacturasState,
    (state: FacturasState)=>state.error,
)

export const loadingFacturas= createSelector(
    selectFacturasState,
    (state: FacturasState)=>state.loading,
)