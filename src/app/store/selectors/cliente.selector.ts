import { createFeatureSelector, createSelector } from "@ngrx/store";
import { ClienteState } from "../reducers";


export const selectClienteState= createFeatureSelector<ClienteState>('cliente');

export const errorCliente= createSelector(
    selectClienteState,
    (state: ClienteState)=>state.error,
)

export const loadingCliente= createSelector(
    selectClienteState,
    (state: ClienteState)=>state.loading,
)