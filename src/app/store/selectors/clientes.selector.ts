import { createFeatureSelector, createSelector } from "@ngrx/store";
import { ClientesState } from "../reducers";

export const selectClientesState= createFeatureSelector<ClientesState>('clientes')

export const selcecionarClientes= createSelector(
    selectClientesState,
    (state: ClientesState)=> state.clientes,
)

export const mostrarError= createSelector(
    selectClientesState,
    (state: ClientesState)=>state.error,
)

export const loadingClientes= createSelector(
    selectClientesState,
    (state: ClientesState)=>state.loading,
)

export const seleccionarFiltrosClientes= createSelector(
    selectClientesState,
    (state: ClientesState)=>state.filtros,
)