import { createFeatureSelector, createSelector } from "@ngrx/store";
import { FacturaState } from "../reducers";

export const selectFacturaState= createFeatureSelector<FacturaState>('factura');

export const mostrarFactura= createSelector(
    selectFacturaState,
    (state: FacturaState)=>state.factura,
)

export const mostrarErrorFactura= createSelector(
    selectFacturaState,
    (state: FacturaState)=>state.error,
)

export const loadingFactura= createSelector(
    selectFacturaState,
    (state: FacturaState)=>state.loading,
)