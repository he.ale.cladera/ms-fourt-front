import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable, concatMap, map, of, switchMap } from 'rxjs';
import { Cliente, PageCliente } from '../interfaces/cliente.inteface';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  private http: HttpClient = inject(HttpClient);
  private URL: String      = "http://localhost:8082";

  constructor() { }

  obtenerClientes( page: number, size: number, estado: boolean):Observable<PageCliente>{
    const params= new HttpParams()
      .set('page', page)
      .set('size', size)
      .set('activo', estado);
    return this.http.get<PageCliente>(`${this.URL}/cliente/listar`, {params});
  }

  obtenerCliente(id: number):Observable<Cliente>{
    const ruta= `${this.URL}/cliente/buscarId`;
    return this.http.get<Cliente>(`${ruta}/${id}`);
  }

  actualizarCliente(cliente: Cliente):Observable<any>{
    const ruta= `${this.URL}/cliente/actualizar`;
    return this.http.put<any>(`${ruta}/${cliente.id}`, cliente);
  }

  crearCliente(cliente: Cliente):Observable<Cliente>{
    const ruta= `${this.URL}/cliente/crear`;
    return this.http.post<Cliente>(ruta, cliente);
    // console.log(cliente)
    // return of(cliente);
  }

  activarDesactivarCliente(id: number, estado: boolean):Observable<any>{
    const ruta= `${this.URL}/cliente/desactivar`;
    return this.http.patch<any>(`${ruta}/${id}`, {estado});
  }

  desacctivarActualizarClientes(idCliente: number, page: number, size: number, estado: boolean): Observable<PageCliente>{
    return this.activarDesactivarCliente(idCliente, !estado).pipe(
      concatMap(()=>this.obtenerClientes(page, size, estado))
    );
  }

  guardarCliente(cliente: Cliente, id: number): Observable<Cliente>{
    const ruta= `${this.URL}/cliente/actualizar/${id}`;
    return this.http.put<Cliente>(ruta, cliente);
  }

  containsOnlyNumbers(str: string): boolean {
    const regex = /^\d+$/;
    return regex.test(str);
  }

  findClientesByIdOrNit(terminoBuscar: string):Observable<string[]>{
    let ruta;
    let params;
    if (this.containsOnlyNumbers(terminoBuscar)){ 
      params= new HttpParams().set("nit", terminoBuscar);
      ruta= `${this.URL}/cliente/buscarNitClientes`;
    }else{
      params= new HttpParams().set("nombre", terminoBuscar);
      ruta= `${this.URL}/cliente/buscarNombreClientes`;
    }
    return this.http.get<string[]>(ruta, {params});
  }

  obtenerClientesNitOrName(terminoBuscar: string, page: number, size: number, estado: boolean):Observable<PageCliente>{
    let params;
    if (this.containsOnlyNumbers(terminoBuscar)){ 
      params= new HttpParams()
        .set('page', page)
        .set('size', size)
        .set('activo', estado)
        .set("nit", terminoBuscar);
       
      return this.http.get<PageCliente>(`${this.URL}/cliente/listar`, {params});
    }else{
      params= new HttpParams()
        .set('page', page)
        .set('size', size)
        .set('activo', estado)
        .set("nombre", terminoBuscar);
        return this.http.get<PageCliente>(`${this.URL}/cliente/listar`, {params});
    }
  }
}
