import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable, concatMap, map } from 'rxjs';
import { Producto, ProductoPage } from '../interfaces/producto.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {
  private http= inject(HttpClient);
  private URL= 'http://localhost:8082';

  constructor() { }

  obtenerProductos(page: number, size: number, disponibilidad: boolean): Observable<ProductoPage>{
    const ruta= `${this.URL}/producto/listar`;
    let params= new HttpParams()
      .set('page', page)
      .set('disponibilidad', disponibilidad)
      .set('size', size);
    return this.http.get<ProductoPage>(ruta, {params});
  }

  obtenerProducto(id: number): Observable<Producto>{
    const ruta= `${this.URL}/producto/buscarId`;
    return this.http.get<Producto>(`${ruta}/${id}`);
  }

  crearProducto(producto: Producto){
    const ruta= `${this.URL}/producto/crear`;
    return this.http.post<any>(ruta, producto);
  }

  actualizarProducto(producto: Producto, id: number){
    const ruta= `${this.URL}/producto/actualizar`;
    return this.http.put<any>(`${ruta}/${producto.id}`, producto);
  }

  eliminarProducto(id: number){
    const ruta= `${this.URL}/producto/eliminar`;
    return this.http.delete<any>(`${ruta}/${id}`);
  }

  setDisponibilidadProducto(id: number, disponibilidad:boolean){
    const ruta= `${this.URL}/producto/disponibilidad/${id}`;
    return this.http.patch(ruta, {disponibilidad});
  }

  setDisponibilidadYObtenerProductos(idProducto: number, page: number, size: number, estado: boolean):Observable<ProductoPage>{
    return this.setDisponibilidadProducto(idProducto, !estado).pipe(
      concatMap(()=>this.obtenerProductos(page, size, estado))
    )
  }

  findProductosName(nombre: string): Observable<Producto[]>{
    const ruta= `${this.URL}/producto/buscar/${nombre}`;
    return this.http.get<Producto[]>(ruta); 
  }

  getProductFactura(nombre: string): Observable<Producto>{
    const ruta= `${this.URL}/producto/buscarParaFactura/${nombre}`;
    return this.http.get<Producto>(ruta); 
  }

  findProductoByNombre(terminoBuscar: string):Observable<string[]>{
    const ruta= `${this.URL}/producto/buscarNombreFiltro`;
    let params= new HttpParams()
      .set('nombre', terminoBuscar);
    return this.http.get<string[]>(ruta, {params});
  }

  obtenerProductosName(terminoBuscar:string, page:number, size:number, disponibilidad:boolean):Observable<ProductoPage>{
    const ruta= `${this.URL}/producto/listar`;
    let params= new HttpParams()
      .set('page', page)
      .set('disponibilidad', disponibilidad)
      .set('size', size)
      .set('nombre',terminoBuscar);
    return this.http.get<ProductoPage>(ruta, {params});
  }
}
