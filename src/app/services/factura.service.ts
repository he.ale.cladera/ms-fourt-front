import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Factura, PageFactura } from '../interfaces/factura.interface';
import { Observable } from 'rxjs';
import { FacturaDetallada } from '../interfaces/FacturaDetallada.inteface';

@Injectable({
  providedIn: 'root'
})
export class FacturaService {
  private http: HttpClient = inject(HttpClient);
  private URL: String      = "http://localhost:8083";
  
  constructor() { }

  obtenerFacturas(page: number, size: number):Observable<PageFactura>{
    let params= new HttpParams()
      .set('page', page)
      .set('size', size);
    return this.http.get<PageFactura>(`${this.URL}/factura/listar`, {params});
  }

  obtenerFactura(id: number):Observable<FacturaDetallada>{
    return this.http.get<FacturaDetallada>(`${this.URL}/factura/obtener/${id}`);
  }
  
  crearFactura(factura: Factura):Observable<Factura>{
    return this.http.post<Factura>(`${this.URL}/factura/crearFactura`,factura);
  }
  actualizarFactura(id: number, factura: Factura):Observable<Factura>{
    return this.http.put<Factura>(`${this.URL}/factura/actualizar/${id}`, factura);
  }

  generarReporte(id: number): Observable<Blob> {
    const headers = new HttpHeaders({ 'Accept': 'application/pdf' });
    return this.http.get(`${this.URL}/factura/generarReporte/${id}`, { headers: headers, responseType: 'blob' });
  }
}
