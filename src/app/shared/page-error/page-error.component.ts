import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Payload } from '../../interfaces/error.interface';

@Component({
  selector: 'app-page-error',
  templateUrl: './page-error.component.html',
  styles: ``,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PageErrorComponent {
  @Input({ required:true })
  public payload!: Payload;
}
