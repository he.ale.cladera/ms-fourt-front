import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styles: ``,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolbarComponent implements OnInit {
  items: MenuItem[] | undefined;

  ngOnInit() {
      this.items = [
          {
              label: 'Productos',
              icon: 'pi pi-home',
              routerLink: 'productos/list'
          },
          {
              label: 'Clientes',
              icon: 'pi pi-users',
              routerLink: 'clientes/list'
          },
          {
              label: 'Facturas',
              icon: 'pi pi-shopping-cart',
              routerLink: 'facturas/list'
          }
      ]
  }

}
