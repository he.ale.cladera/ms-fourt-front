import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { PrimengModule } from '../primeng/primeng.module';
import { TitleComponent } from './title/title.component';
import { PageErrorComponent } from './page-error/page-error.component';
import { PageLoadingComponent } from './page-loading/page-loading.component';

@NgModule({
  declarations: [
    ToolbarComponent,
    TitleComponent,
    PageErrorComponent,
    PageLoadingComponent
  ],
  imports: [
    CommonModule,
    PrimengModule,
  ],
  exports: [
    ToolbarComponent,
    TitleComponent,
    PageErrorComponent,
    PageLoadingComponent,
  ]
})
export class SharedModule { }
