import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaginatorModule } from 'primeng/paginator';
import { DataViewModule } from 'primeng/dataview';
import { ButtonModule } from 'primeng/button';
import { TagModule } from 'primeng/tag';
import { MenubarModule } from 'primeng/menubar';
import { TableModule } from 'primeng/table';
import { SpeedDialModule } from 'primeng/speeddial';
import { ToastModule } from 'primeng/toast';
import { ToolbarModule } from 'primeng/toolbar';
import { SplitButtonModule } from 'primeng/splitbutton';
import { InputTextModule } from 'primeng/inputtext';
import { ImageModule } from 'primeng/image';
import { SelectButtonModule } from 'primeng/selectbutton';

import { StepperModule } from 'primeng/stepper';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { IconFieldModule } from 'primeng/iconfield';
import { InputIconModule } from 'primeng/inputicon';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { PanelModule } from 'primeng/panel';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  exports: [
    PaginatorModule,
    DataViewModule,
    ButtonModule,
    TagModule,
    MenubarModule,
    TableModule,
    SpeedDialModule,
    ToastModule,
    ToolbarModule,
    SplitButtonModule,
    InputTextModule,
    ImageModule,
    SelectButtonModule,
    StepperModule,
    ToggleButtonModule,
    IconFieldModule,
    InputIconModule,
    AutoCompleteModule,
    PanelModule,
  ]
})
export class PrimengModule { }
